package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.LinkedList;

public class Tweet extends ActiveRecord {

	private Integer tweet_id = -1; 
	private Integer user_id = -1;
	private String user_name = null;
	private String text;
	private Integer likes;
	private String time;

	public Integer getTweet_id() {
		return tweet_id;
	}


	private void setTweet_id(Integer tweet_id) {
		this.tweet_id = tweet_id;
	}


	public Integer getUser_id() {
		return user_id;
	}


	private void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}


	public String getUser_name() {
		return user_name;
	}


	private void setUser_name(String user_name) {
		this.user_name = user_name;
	}


	public String getText() {
		return text;
	}


	private void setText(String text) {
		this.text = text;
	}


	public Integer getLikes() {
		return likes;
	}


	public void setLikes(Integer likes) {
		this.likes = likes;
	}


	public String getTime() {
		return time;
	}


	private void setTime(String time) {
		this.time = time;
	}

	public static Collection<Tweet> findAll() throws ModelException
	{
		LinkedList<Tweet> result = new LinkedList<Tweet>();
		try {
			Statement stmt = getDbConnection().createStatement();
			String query = "select t.*, u.usNAME from tweets t natural join users u order by twTIME desc";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				Tweet tweet = new Tweet();
				tweet.setTweet_id(rs.getInt(1));
				tweet.setUser_id(rs.getInt(2));
				tweet.setText(rs.getString(3));
				tweet.setLikes(rs.getInt(4));
				tweet.setTime(rs.getString(5));
				tweet.setUser_name(rs.getString(6));
				result.add(tweet);
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException ex) { throw new ModelException(ex); }
		return result;		
	}

	public static Collection<Tweet> findByUserId(Integer user_id) throws ModelException
	{
		LinkedList<Tweet> result = new LinkedList<Tweet>();
		try {
			Statement stmt =  getDbConnection().createStatement();
			String query = "select * from tweets where usID = "+user_id+" order by twTIME desc";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				Tweet tweet = new Tweet();
				tweet.setTweet_id(rs.getInt(1));
				tweet.setUser_id(rs.getInt(2));
				tweet.setText(rs.getString(3));
				tweet.setLikes(rs.getInt(4));
				tweet.setTime(rs.getString(5));
				result.add(tweet);
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException ex) { throw new ModelException(ex); }
		return result;		
	}

	public static Tweet findById(Integer id) throws ModelException
	{
		Tweet tweet = null;
		try {
			Statement stmt =  getDbConnection().createStatement();
			String query = "select * from tweets where twID = "+id;
			ResultSet rs = stmt.executeQuery(query);
			if (rs.first()) {
				tweet = new Tweet();
				tweet.setTweet_id(rs.getInt(1));
				tweet.setUser_id(rs.getInt(2));
				tweet.setText(rs.getString(3));
				tweet.setLikes(rs.getInt(4));
				tweet.setTime(rs.getString(5));
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException ex) { throw new ModelException(ex); }
		return tweet;
	}

	public static Tweet create(Integer user_id, String text) throws ModelException {
		Tweet tweet = null;
		try {
			String insert = "insert into tweets (usID, twTEXT) values (?, ?)";
			PreparedStatement stmt =  getDbConnection().prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, user_id);
			stmt.setString(2, text);
			stmt.executeUpdate();
			ResultSet keys = stmt.getGeneratedKeys();
			keys.first();
			tweet = Tweet.findById(keys.getInt(1));
			keys.close();
			stmt.close();
		}
		catch (SQLException ex) { throw new  ModelException(ex);}

		return tweet;
	}

	public void update() throws ModelException {

		String update = "update tweets set twLIKES = ? where twID = ?";
		try {
			PreparedStatement stmt =  getDbConnection().prepareStatement(update);
			stmt.setInt(1, this.getLikes());
			stmt.setInt(2, this.getTweet_id());
			stmt.executeUpdate();
			stmt.close();
		}
		catch (SQLException ex) { throw new ModelException(ex); }
	}
	
	public void delete() throws ModelException {

		String delete = "delete from tweets where twID = ?";
		try {
			PreparedStatement stmt =  getDbConnection().prepareStatement(delete);
			stmt.setInt(1, this.getTweet_id());
			stmt.executeUpdate();
			stmt.close();
		}
		catch (SQLException ex) { throw new ModelException(ex); }
	}
}
